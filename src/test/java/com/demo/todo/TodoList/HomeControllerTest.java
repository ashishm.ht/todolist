package com.demo.todo.TodoList;
 
import java.util.ArrayList;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import  org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.demo.todo.TodoList.controller.HomeController;
import com.demo.todo.TodoList.pojo.Task;
import com.demo.todo.TodoList.service.TaskServiceInterface;

@RunWith(SpringJUnit4ClassRunner.class)
public class HomeControllerTest {

	private MockMvc mockMvc;
	ArrayList<Task> tasks=new ArrayList<Task>();
	@Mock
	TaskServiceInterface taskService; 
	
	@InjectMocks
	private HomeController homeController;

	@Before
	public void setUp() {
		Mockito.when(taskService.getTask(1)).thenReturn(new Task(1, "darshan"));
		mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();	
	}
	
	@Test
	public void testSayHello() throws Exception {
		 mockMvc.perform(MockMvcRequestBuilders.get("/hi"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Hello from spring"));	
	}
	

	@Test
	public void getAll_getTaskJsonWhenHitUrl() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/task/1").
				 accept(MediaType.APPLICATION_JSON))
		 		.andExpect(MockMvcResultMatchers.status().isOk())
		 		.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("darshan")));
	}
}
