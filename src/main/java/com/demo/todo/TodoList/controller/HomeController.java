package com.demo.todo.TodoList.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.todo.TodoList.pojo.Task;
import com.demo.todo.TodoList.service.TaskServiceInterface;
import com.demo.todo.TodoList.service.impl.TaskServiceImpl;

@RestController
public class HomeController {

	@Autowired
	TaskServiceInterface taskService;

	@RequestMapping("/hi")
	public String sayHi() {
		return "Hello from spring";
	}

	@GetMapping("/tasks")
	public ArrayList<Task> getAll() {
		return taskService.getAll();
	}
	
	@PostMapping("/task")
	public Task addTask(@RequestBody Task task) {
		taskService.add(task);
		return task;
	}
	
	@PostMapping("/tasks")
	public ArrayList<Task> addMultiTask(@RequestBody ArrayList<Task> tasks) {
		
		return taskService.addAll(tasks);
	}
	
	
	
	@GetMapping("/task/{id}")
	public Task getTask(@PathVariable int id) {
		return taskService.getTask(id);
	}

}
