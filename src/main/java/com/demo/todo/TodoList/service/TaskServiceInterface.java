package com.demo.todo.TodoList.service;

import java.util.ArrayList;

import com.demo.todo.TodoList.pojo.Task;

public interface TaskServiceInterface {
	public ArrayList<Task> getAll();
	public Task add(Task task);
	public Task getTask(int id);
	public ArrayList<Task> addAll(ArrayList<Task> tasks);
}
