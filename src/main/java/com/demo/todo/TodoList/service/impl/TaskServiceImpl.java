package com.demo.todo.TodoList.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.todo.TodoList.pojo.Task;
import com.demo.todo.TodoList.repo.TaskRepo;
import com.demo.todo.TodoList.service.TaskServiceInterface;

@Service
public class TaskServiceImpl implements TaskServiceInterface {
	@Autowired
	TaskRepo repo;

	public static ArrayList<Task> tasks = new ArrayList<>();

	@Override
	public ArrayList<Task> getAll() {
		return (ArrayList<Task>) repo.findAll();
	}

	@Override
	public Task add(Task task) {
		return repo.save(task);
	}

	@Override
	public Task getTask(int id) {
		 
		return repo.getTaskById(id);
	}

	@Override
	public ArrayList<Task> addAll(ArrayList<Task> tasks) {
		 
		return (ArrayList<Task>) repo.saveAll(tasks);
	}
}
