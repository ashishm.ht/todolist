package com.demo.todo.TodoList.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.todo.TodoList.pojo.Task;

public interface TaskRepo extends JpaRepository<Task, Integer>{

	public Task getTaskById(int id);
}
